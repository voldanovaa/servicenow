package com.cgi.sem.servicenow.controller;

import com.cgi.sem.servicenow.exception.ResourceNotFoundException;
import com.cgi.sem.servicenow.model.Ticket;
import com.cgi.sem.servicenow.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class TicketController {


    @Autowired
    private TicketRepository ticketRepository;


    //get all tickets
    @GetMapping("tickets")
    public List<Ticket> getAllTickets(){
        return this.ticketRepository.findAll();
    }

    //get ticket by id
    @GetMapping("/tickets/{id}")
    public ResponseEntity<Ticket> getTicketById(@PathVariable(value = "id") Long ticketId)
            throws ResourceNotFoundException {
        Ticket ticket = ticketRepository.findById(ticketId)
                .orElseThrow(() -> new ResourceNotFoundException("Ticket not found for this id :: " + ticketId));
        return ResponseEntity.ok().body(ticket);
    }
//    @GetMapping("/tickets/{name}")
//    public List<Ticket> getTicketByName(){
//        return this.ticketRepository
//    }


    //save ticket
    @PostMapping("tickets")
    public Ticket createTicket(@RequestBody Ticket ticket){
        return this.ticketRepository.save(ticket);
    }


}
