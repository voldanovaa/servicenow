package com.cgi.sem.servicenow.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "ticket")
@ApiModel(description="All details about the ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    @Column(name="name")
    @ApiModelProperty(notes="Ticket name")
    private String name;

    @Column(name="email")
    private String email;

    @Column(name="id_person_creator")
    private long idCreator;

    @Column(name="id_person_assigned")
    private long idAssigned;

    @Column(name="creation_datetime")
    private LocalDate creationDateTime;

    public Ticket(){}

    public Ticket(String name, String email, long idCreator,
                  long idAssigned, LocalDate creationDateTime) {
        this.name = name;
        this.email = email;
        this.idCreator = idCreator;
        this.idAssigned = idAssigned;
        this.creationDateTime = creationDateTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getIdCreator() {
        return idCreator;
    }

    public void setIdCreator(long idCreator) {
        this.idCreator = idCreator;
    }

    public long getIdAssigned() {
        return idAssigned;
    }

    public void setIdAssigned(long idAssigned) {
        this.idAssigned = idAssigned;
    }

    public LocalDate getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDate creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", idCreator=" + idCreator +
                ", idAssigned=" + idAssigned +
                ", creationDateTime=" + creationDateTime +
                '}';
    }
}
